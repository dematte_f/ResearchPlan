
% 3.	Premiers résultats / First results: 1-2 pages

\hspace*{0.3cm}

%Until the end of October 2020, the topic of this PhD thesis was the study of quench protection and hot spot temperature in accelerator magnets. This was a conjoint project with CERN within the CHART collaboration. Specifically, the analysis concerned the effects of thermal stress and strain during a quench in the impregnated CERN’s MQXF cable, which is used for the high luminosity upgrade of LHC. As of October 2020, the research on this topic consisted mainly in numerical modelling of the Rutherford cable MQXF at the strand level to be used later on for quench simulations. Unfortunately, the approval of this project between EPFL and CERN has been delayed and the topic of this PhD research had to be changed after four months of work.

There are two possibilities to produce a Nb$_3$Sn-based magnet: Wind \& React (W\&R) and React \& Wind (R\&W). As the names already suggest, in the first method the heat treatment takes place after winding while in the second the coil winding happens after heat treatment of the superconductor. In order to minimize the bending stress in a R\&W cable during the handling after heat treatment (i.e. straightening for jacketing and winding), the superconductor must be as close as possible to the neutral bending line thus resulting in a flat cable design (see Figure \ref{fig:RW2_Schliff}). The SPC is specialized in R\&W cables for fusion and this is the design investigated in this PhD thesis.

To gain the experience necessary to design the DEMO TF high-current superconducting cable, we develop a R\&W prototype called RW3. The R\&W technology is quite interesting for the development of Nb$_3$Sn based CICCs for fusion magnets because it allows to reduce the thermal strain on the Nb$_3$Sn strands resulting in a higher performance of the superconductor.

The RW3 design, based on the successful development of RW2 \cite{Bruzzone2019} (Figure~\ref{fig:RW2_Schliff}), is completed and its production process has started. I participated to a meeting with the Italian cabling company TRATOS for negotiations regarding the manufacturing of the outer Rutherford stabilizer and the two-stage flat superconducting cable (see Figure~\ref{fig:RW2_Schliff} for reference). Additionally, negotiations and discussions with possible providers for the stainless steel jacket have been carried out and the jacket is being now produced by the Italian company Calvi.

\subsection{Optimization of the Strand Heat Treatment}

To be able to propose a design for the high current conductor, a heat treatment optimization was performed to find the highest possible J$_c$ for $\varnothing=1\,$mm internal tin strands provided by Kiswire Advanced Technology (KAT, Korea). Four heat treatments have been investigated by changing only the duration of the $650^\circ$C step: 100h, 120h, 150h and 170h. To test also for reproducibility, for each heat treatment four samples were prepared. An example for these samples can be found in Figure~\ref{fig:Barrel}.

\begin{figure}[h]
	\centering
	\includegraphics[width=.6\linewidth]{figures/ITER_Barrel2.png}
	\caption{Photo of one sample before undergoing heat treatment. The Cr coating has been etched away to facilitate soldering of the voltage taps for data acquisition during the measurements.}
	\label{fig:Barrel}
\end{figure}

The measurements were performed at $4.2\,$K for fields in the range from $9\,$T to $15\,$T and repeated twice for each sample. The data show the rise of an electrical field depending on the applied current and external magnetic field. This field can be expressed as 

\begin{equation}\label{eq:Ic}
	E = E_c \left(\frac{I}{I_c}\right)^n\quad ,
\end{equation}

where $E_c = 0.1\,\mu$V/cm is the electric field defining to the strand's critical current ($I_c$) and n is the so-called n factor which describes the sharpness of the transition. Fitting eq.~\eqref{eq:Ic} to the measured data both the critical current and the n factor can be obtained. An example is shown in Figure~\ref{fig:DataFit} for a measurement at $12\,$T and a heat treatment with 150h plateau. The resulting I$_c$, J$_c$ and n-factor are reported in the title.

\begin{figure}[h]
	\centering
	\includegraphics[width=.8\linewidth]{E:/PostProcessingPlots/Jc_15T/SK001AD1/51/150Std_51_12T_b_zoom.pdf}
	\caption{Fit for the determination of the critical current on a measurement performed at $12\,$T on a sample with a 150h plateau at $650^\circ$C. Critical current, critical current density and n-factor can be found in the title. The red dots represent the measured data while the fit is plotted in blue. The value of the critical electric field $E_c$ is highlighted by a horizontal line.}
	\label{fig:DataFit}
\end{figure}

To identify the heat treatment that leads to the best electrical performances, an average J$_c$ is calculated among all samples with the same $650^\circ$C plateau. The statistic uncertainty is calculated as the standard deviation. As shown in Figure~\ref{fig:JcPeak}, the highest J$_c$ is reached by the strands which underwent a heat treatment with a hot plateau of 150h, although the performance peak is quite flat. A summary of the results is shown in Figure~\ref{fig:JcPeak}.

\begin{figure}[h]
	\centering
	\includegraphics[width=.7\linewidth]{figures/Jc_peak}
	\caption{Average J$_c$ at $12\,$T and $4.2\,$K for all heat treatment cycles,the errorbars represent the standard deviations on each measurement series. The best performance is reached by the sample with 150h plateau at $650^\circ$C.}
	\label{fig:JcPeak}
\end{figure}

%\begin{table}[h]
%	\centering
%	\caption{Result summary for the J$_c$ measurements with confidence interval.}
%	\begin{tabular}{c|c|c}
%		Heat Treatment & J$_c (4.2\,\mathrm{K},12\,\mathrm{T})$ & Confidence Interval \\ \hline\hline
%		100h & 1239.5 A/mm$^2$ & 4.0$\,\sigma$\\ \hline
%		120h & 1287.6 A/mm$^2$ & 1.2$\,\sigma$\\ \hline
%		150h & 1296.0 A/mm$^2$ & reference\\ \hline
%		170h & 1290.0 A/mm$^2$ & 0.5$\,\sigma$\\ 
%	\end{tabular}
%	\label{tab:JcSummary}
%\end{table}

\subsection{Preliminary highest grade design}

To determine the Nb$_3$Sn cross-section for $I_{op}~=~104.95\,$kA and a temperature margin of T$~=~(4.5+2)\,$K, ITER scaling law in \cite{Godeke2006} for Nb$_3$Sn is used, leading to $\sigma_{Nb_3Sn}~=~145.4\,\mathrm{mm}^2$. Considering a two stage flat cable with first stage sub-cables made of 19 strands and $1\,$mm strands with a Cu/non-Cu ratio of $1:1$, the targeted superconductor's cross section is achieved cabling 20 first stage sub-cables in a Rutherford-like flat geometry with 20\% void fraction.

The total amount of copper needed for quench protection is calculated considering a current density flowing through the copper $J_{Cu}~=~93.4\,\mathrm{A/mm^2}$ and dividing $I_{op}$ by it. Since the strands are made by 50\% of copper, the net copper cross section needed turns out to be $1124\,\mathrm{mm}^2$. To ensure proximity of the superconductor to the neutral bending axis, the copper is divided in two Rutherford cables with $10\%$ void fraction each made by 22 wires with $\varnothing~=~5.3\,$mm. 

The presented design is shown in Figure~\ref{fig:PreliminaryDesign}.

\begin{figure}[h]
	\centering
	\begin{tikzpicture}[scale=0.8,every node/.style={scale=0.8}]
%		\begin{scope}
%			\draw[help lines,xstep=.5,ystep=.5] (0,0) grid (15,7);
%			\foreach \x in {0,5,...,50} { \node [anchor=north] at (\x/10,0) {\x}; }
%			\foreach \y in {0,5,...,50} { \node [anchor=east] at (0,\y/10) {\y}; }
			\draw[black,thick] (.5,6) circle (6mm);
			\node[anchor=south] (strand) at (.5,6.6) {strand $1\,$mm};
			\node[right= 1cm of strand, yshift=-1cm] (subcable) {
				\begin{tikzpicture}[scale=0.8,every node/.style={scale=0.8}]
			\draw[black,thick] (3.5,4) circle (3mm);
			\draw[black,thick] (2.9,4) circle (3mm);
			\draw[black,thick] (3.2,3.48) circle (3mm);
			\draw[black,thick] (3.8,3.48) circle (3mm);
			\draw[black,thick] (4.1,4) circle (3mm);
			\draw[black,thick] (3.8,4.52) circle (3mm);
			\draw[black,thick] (3.2,4.52) circle (3mm);
			\draw[black,thick] (3.5,2.96) circle (3mm);
			\draw[black,thick] (4.1,2.96) circle (3mm);
			\draw[black,thick] (2.9,2.96) circle (3mm);
			\draw[black,thick] (3.5,5.04) circle (3mm);
			\draw[black,thick] (4.1,5.04) circle (3mm);
			\draw[black,thick] (2.9,5.04) circle (3mm);
			\draw[black,thick] (4.4,4.52) circle (3mm);
			\draw[black,thick] (2.6,4.52) circle (3mm);
			\draw[black,thick] (4.4,3.48) circle (3mm);
			\draw[black,thick] (2.6,3.48) circle (3mm);
			\draw[black,thick] (4.7,4) circle (3mm);
			\draw[black,thick] (2.3,4) circle (3mm);
		\end{tikzpicture}};
			\foreach \x in {1,2,...,10} {\draw[black,thick] (6+\x*1,6.5) circle (5mm);}
			\foreach \x in {1,2,...,10} {\draw[black,thick] (6+\x*1,5.5) circle (5mm);}
			\node[anchor=south] (first stage) at (4,7.) {first stage subcable};
			\foreach \x in {0,1,...,10} {\draw[red,thick] (\x*0.6,2.) circle (3mm);}
			\foreach \x in {0,1,...,10} {\draw[red,thick] (\x*0.6,1.4) circle (3mm);}
			\draw[red,ultra thick,rounded corners] (9,2.5) rectangle (15.5,3.5);
			\draw[black,ultra thick,rounded corners] (9,2.5) rectangle (14,1.);
			\draw[red,ultra thick,rounded corners] (9,1) rectangle (15.5,0);
			\draw[gray,ultra thick,rounded corners] (14,1.) rectangle (15.5,2.5);
			\node[anchor=south] (stabilizer) at (3,2.5) {stabilizer out of 22 wires};
			\node[anchor=south] (sc) at (11.5,7.1) {second stage cable out of 20 subcables};
			\node[anchor=south] (layout) at (12.5,3.7) {layout inside the jacket};
%		\end{scope}
	\end{tikzpicture}
	\caption{Sketch for the preliminary design of the $104.95\,$kA CICC. The superconductor is wound in a two-stage flat cable made of 20 subcables. Each subcable is composed by 19 strands. The stabilizer is made of two Rutherford cables, each with 22 CuNi-coated Cu wires with two rounded corners (missing in the picture). The schematic layout of the assembled cable with the cooling channel (gray) is shown on the lower right.}
	\label{fig:PreliminaryDesign}
\end{figure}

%\FloatBarrier
\subsection{Mechanical Analysis}

To investigate the mechanical stability of the stainless steel jacket a mechanical analysis was performed using the simulation software ANSYS. For the calculation, the TF coil case geometry is taken from the DEMO Baseline 2018 while both operation conditions and jacket thickness are taken from case \#7 in \cite{Wesche2021}. Figure~\ref{fig:Jacket} shows a half profile of the jacket in the highest field layer. The code used for the simulation was developed at the \textit{Forschungszentrum J\"ulich} and has been presented in \cite{Ivashov2020} and allows the calculation of the membrane and bending stress on the jacket walls during operation. This is done with a 2D analysis of the inner leg of the TF coil.

\begin{figure}[h]
	\centering
	\includegraphics[width=.6\linewidth]{G:/Dematte/Reports/BigOne_Jacket}
	\caption{Half profile of the jacket in the highest field layer with dimensions. The inner corners have a radius of $6\,$mm, to reduce the peak stress.}
	\label{fig:Jacket}
\end{figure}

To assure mechanical stability for stainless steel at $4.2\,$K, the allowable design limits are $667\,$MPa and $867\,$MPa respectively for membrane and membrane + bending stress based on the Tresca stress criterion. As it can be seen in Figure~\ref{fig:StressPlot}, all layers lay far below the design limits, thus allowing the possibility to investigate thinner jacket walls.

\begin{figure}[h]
	\centering
	\includegraphics[width=.7\linewidth]{E:/TF_coil/demo-tf-coil-dimensioning-NewGeometry/StressResults/MembendStress_comparison_noseParametric.pdf}
	\caption{Plot for the membrane stress and membrane $+$ bending stress on the TF coil layers during operation. All values are well below the design limits.}
	\label{fig:StressPlot}
\end{figure}

