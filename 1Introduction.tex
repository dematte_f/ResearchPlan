% 1.	Introduction (cadre général) / Introduction (general framework): 1 page

\hspace*{0.3cm}
\begin{comment}
The necessity to reduce CO$_2$ emission has driven the research towards CO$_2$ emission-free energy production. Among the most promising options is nuclear fusion, which may become an alternative carbon-free source of energy in not too long time. 

To reach and control nuclear fusion on Earth, one possibility is to confine the plasma in a torus with high intensity B-fields. The device used in this case is made of a toroidal vacuum chamber surrounded by high field magnets to ensure plasma confinement.

An example of such a device is ITER, which will prove the feasibility of the production of net energy from nuclear fusion. While ITER has just entered its assembly phase, the fusion community is already working on the design and R\&D of the following step. The Demonstration Fusion Reactor (DEMO) aims at showing the industrial feasibility of fusion power plants as energy source by providing a high fusion gain (Q~$=~\frac{P_f^{out}-P^{in}}{P^{in}}~\approx~25$) and a high thermonuclear power output ($P_f^{out}$) around $2\,$GW \cite{Federici2018}. To achieve these goals, among many factors, a steady state toroidal B-field of roughly $6\,$T \cite{Federici2018} is needed in the plasma (European approach). Due to inverse scaling of the B-field with the distance from the coil, the magnetic field at the conductors surface is~$\approx~12-13\,$T. To provide such steady-state field, superconducting cables are used to build the D-shape toroidal field (TF) coils. 
\end{comment}
In fusion devices based on high magnetic fields and long pulse-length (e.g. ITER and DEMO), the cables are made of superconducting strands (Nb$_3$Sn for the TF system) coupled to copper wires used for quench protection and flow of supercritical helium for thermal stability. All components are then enclosed in a jacket, usually made of stainless steel, which serves both as container for the helium flow and as structural support for the mechanical loads. Because of the layout, this type of cable is called cable-in-conduit conductor (CICC). An example of a CICC design developed at the Swiss Plasma Center (SPC) can be found in Figure~\ref{fig:RW2_Schliff}.

There are two possibilities to produce a Nb$_3$Sn-based magnet: Wind \& React (W\&R) and React \& Wind (R\&W). As the names already suggest, in the first method the heat treatment takes place after winding, while in the second the coil winding happens after heat treatment of the superconductor. In order to minimize the bending stress in a R\&W cable during the handling after heat treatment (i.e. straightening for jacketing and winding), the superconductor must be as close as possible to the neutral bending line, thus resulting in a flat cable design (see Figure \ref{fig:RW2_Schliff}). 

The main advantages of choosing a R\&W approach are the reduction of residual strain on the Nb$_3$Sn strands (and thus higher performances) and the possibility to have a complex jacket design with different wall thicknesses in toroidal and radial direction. Both aspects lead to the reduction of the radial built w.r.t. to a design with a W\&R approach, such as the one used for ITER Nb$_3$Sn coils.

The SPC is specialized in R\&W cables for fusion and this is the design investigated in this EUROfusion Engineering Grant.

\begin{figure}[h]
	\centering
	\begin{tikzpicture}[thick,scale=0.8, every node/.style={scale=0.8}]
		\node [anchor=west] (stabilizer) at (-1.7,4.5) {stabilizer};
%		\node [anchor=west] (cc) at (-1.8,2.5) {cooling channel};
		\node [anchor=east] (sc) at (13.5,4) {superconductor};
		\begin{scope}[xshift=1cm]
			\node[anchor=south west,inner sep=0] (image) at (0,0)
			{\includegraphics[width=.5\linewidth]{figures/RW2_Schliff}};
			\begin{scope}[x={(image.south east)},y={(image.north west)}]
%				\draw[help lines,xstep=.1,ystep=.1] (0,0) grid (1,1);
%				\foreach \x in {0,1,...,9} { \node [anchor=north] at (\x/10,0) {0.\x}; }
%				\foreach \y in {0,1,...,9} { \node [anchor=east] at (0,\y/10) {0.\y}; }
				\draw[red,ultra thick,rounded corners] (0.14,0.61) rectangle (0.89,0.75);
				\draw[red,ultra thick,rounded corners] (0.14,0.26) rectangle (0.89,0.39);
				\draw[yellow,ultra thick,rounded corners] (0.36,0.6) rectangle (0.88,0.395);
				\draw[-latex,red,ultra thick] (stabilizer) to[bend left] (0.12,0.68);
				\draw[-latex,red,ultra thick] (stabilizer) to[bend right] (0.12,0.32);
				\draw[-latex,yellow,ultra thick] (sc) to (0.89,0.5);
			\end{scope}
		\end{scope}
	
	\end{tikzpicture}
	\caption{Cross section of the RW2 prototype developed at the SPC. In red the Cu stabilizer is highlighted while the yellow box indicates the superconductor. Jacket and cooling channel are both made of stainless steel.}
	\label{fig:RW2_Schliff}
\end{figure}

\begin{comment}
In the case of DEMO, the target magnetic field imposes the use of Nb$_3$Sn as the required NbTi cross-section would be too large for fields above $\approx 7-8\,$T at $4.2\,$K. This limitation on the superconducting material is due to the so-called ``critical surface'', as shown in Figure~\ref{fig:CriticalSurface}. This surface is defined by the critical temperature (T$_c$), the critical field (B$_c$) and critical current density (J$_c$) flowing through the superconductor. Above these values, the material exits the superconducting state and becomes a resistive conductor (normal state). 

\begin{figure}[h]
	\centering
	\includegraphics[width=.7\linewidth]{E:/PostProcessingPlots/Jc_15T/Jc_plots/ScalingLaws4K.png}
	\caption{Comparison of the critical surface of NbTi (red) and Nb$_3$Sn (cyan) as function of temperature and applied magnetic field. It can be seen clearly that for an applied field of $8\,$T the current density for Nb$_3$Sn is larger than for NbTi.}
	\label{fig:CriticalSurface}
\end{figure}


The use of Nb$_3$Sn as superconductor comes with its own challenges. Nb$_3$Sn is an inter-metallic compound, which forms through solid-state diffusion at 650$^\circ$C. This means that the cable needs to undergo a heat treatment. Nb$_3$Sn is also brittle and strain-sensitive, therefore difficult to handle. 
\end{comment}
Whereas some parameters, such as the required magnetic field in the plasma are set by plasma physics, magnet designers specify other parameters taking into account safety, performance and cost optimization. The development of a coil with low discharge voltage is needed for safety reasons as well as for reducing the number of coil feeders. For a given amount of Ampere Turns (AT), there are two ways to reduce the discharge voltage: increasing the time discharge constant or decreasing the coil inductance.

For a larger time discharge constant, the copper cross-section in the cable must be increased, such that the hot spot temperature reached during the discharge can be kept within acceptable limits. The inductance of a coil is proportional to $N^2$, where N is the number of turns. Thus, for a given total TF current, decreasing the number of turns corresponds to higher current flowing through each turn, which results in the inductance being proportional to $I_{turn}^{-2}$ . Increasing the current will have a quadratic impact on the inductance and a linear impact on the discharge voltage making the design of a high-current ($\approx105\,$kA) CICC conductor attractive for the DEMO reactor \cite{Wesche2021}.

To make sure that a proposed cable design is suitable for a fusion device, DC tests need to be performed in operating-like conditions. For magnetic fields up to $11\,$T, these are performed in SULTAN \cite{Bruzzone2002}, the largest test facility for forced-flow cooled CICC for fusion. The characterization tests usually include the determination of the critical current (I$_c$) and the current sharing temperature (T$_{cs}$) at the conductor nominal operation current I$_{op}$ and effective field up to $B~=~12\,$T. T$_{cs}$ is the temperature at which the superconductor is not capable anymore to transport all the current, and part of the current needs to go through the the copper in the strands and voltage develops.

Depending on the magnet's winding design, it is possible to ``grade'' the superconductor used in each layer. In this context ``grading'' means to optimize the amount of superconductor and steel across the coil winding pack based on the exact needs at each location (i.e. B-field and mechanical stress). This allows to reduce material costs since the high performing and more expensive Nb$_3$Sn can be used for the high field region while the outer magnet layers can be made of the cheaper NbTi. Another advantage of grading is the more efficient use of the available space. Scaling the amount of superconductor and steel needed in the radial direction also leads to a smaller radial built.

%From the prospective of plasma physics, a high field fusion reactor is very attractive because it allows for a better plasma performance. This is linked to the fusion power proportionality to the toroidal field $p_f\propto B_0^4$.

For this reason a design of a R\&W high current TF conductor is considered interesting in the R\&D phase of the EUROfusion DEMO Tokamak. 


