% 2.	Objectifs / Objectives: ½ page

\hspace*{0.3cm}

The TF coils of the EUROfusion DEMO will carry $\sim60\%$ larger ampere-turn (AT) than the ITER TF coils. However, the TF winding packs proposed so far for the EU DEMO have considered a conductor current in the same range of ITER (68 kA). This results in a $~3.5$ times larger coil inductance ($L$), and $~3$ larger stored a magnetic energy ($E$) compared to the ITER TF coils, since $E = 1/2\times L \times I_{cond}^2$. Furthermore, the higher is the inductance, the higher is the discharge voltage. In order to limit the discharge voltage to a reasonable safe value (in terms of avoiding degradation of the magnet insulation, which affects the magnet integrity), it is highly desirable reduce the coil inductance, which can be achieved by operation at larger currents. Increasing the current will have a quadratic impact on the inductance and a linear impact on the discharge voltage making the design of a high-current ($I_{cond} > 100\,$kA) conductor attractive for the DEMO tokamak \cite{Wesche2021}.

This EUROfusion Engineering Grant will explore the use of high current ($\sim105\,$kA) conductors for the DEMO TF coils. The main objectives will be: 1) to propose a high current TF winding pack design, including the associated mechanical, electrical, and thermal-hydraulic analyses; and 2) to design, produce and test a high current conductor prototype in the SULTAN test facility \cite{Bruzzone2002}.

The TF winding pack will be wound in layers, which allows the possibility of superconductor and  steel ``grading'' (i.e., the amount of superconductor and steel will be determined in each layer of the winding pack based on the exact magnetic field and cumulative Lorentz force at each location). This allows reducing material costs since the high performing Nb$_3$Sn can be used for the high field region whereas the outer magnet layers can be made of the cheaper Nb-Ti. Another advantage of grading is the more efficient use of the available space. Scaling the amount of superconductor and steel needed leads to a smaller radial built, which opens up the possibility of considering two winding pack designs: 1) a compact design able to generate the nominal toroidal magnetic field required for DEMO, and 2) a design that makes use of the space savings to increase the coil ampere-turn and generate a higher toroidal magnetic field, which will improve plasma confinement.

The layout of the high current conductor will be similar to the forced-flow conductor shown in Figure~\ref{fig:RW2_Schliff}, which was developed at the Swiss Plasma Center (SPC). The cable will be made of Nb$_3$Sn superconducting strands coupled to copper wires used for quench protection and a flow of supercritical helium for thermal stability. All components will be then enclosed in a steel jacket, which serves both as container for the helium flow and as structural support for the mechanical loads.

The Nb$_3$Sn cable will follow the React \& Wind (R\&W) manufacturing process. As the name suggests, with this method the coil winding happens after heat treatment of the superconductor. The main advantages of the R\&W method are the reduction of residual strain on the Nb$_3$Sn strands (thus higher performance) and the possibility to have a complex jacket design with different wall thicknesses in the toroidal and radial directions. However, one shall be particularly careful during handling of the reacted cable. In order to minimize the bending stress in an R\&W cable during handling after heat treatment (i.e. straightening for jacketing and winding), the superconductor must be as close as possible to the neutral bending line, which results in a flat cable design (see Figure \ref{fig:RW2_Schliff}).

\begin{figure}[h]
	\centering
	\begin{tikzpicture}[thick,scale=0.8, every node/.style={scale=0.8}]
		\node [anchor=west] (stabilizer) at (-1.7,4.5) {stabilizer};
%		\node [anchor=west] (cc) at (-1.8,2.5) {cooling channel};
		\node [anchor=east] (sc) at (13.5,4) {superconductor};
		\begin{scope}[xshift=1cm]
			\node[anchor=south west,inner sep=0] (image) at (0,0)
			{\includegraphics[width=.5\linewidth]{figures/RW2_Schliff_resize}};
			\begin{scope}[x={(image.south east)},y={(image.north west)}]
%				\draw[help lines,xstep=.1,ystep=.1] (0,0) grid (1,1);
%				\foreach \x in {0,1,...,9} { \node [anchor=north] at (\x/10,0) {0.\x}; }
%				\foreach \y in {0,1,...,9} { \node [anchor=east] at (0,\y/10) {0.\y}; }
				\draw[red,ultra thick,rounded corners] (0.14,0.61) rectangle (0.89,0.75);
				\draw[red,ultra thick,rounded corners] (0.14,0.26) rectangle (0.89,0.39);
				\draw[yellow,ultra thick,rounded corners] (0.36,0.6) rectangle (0.88,0.395);
				\draw[-latex,red,ultra thick] (stabilizer) to[bend left] (0.12,0.68);
				\draw[-latex,red,ultra thick] (stabilizer) to[bend right] (0.12,0.32);
				\draw[-latex,yellow,ultra thick] (sc) to (0.89,0.5);
			\end{scope}
		\end{scope}
	
	\end{tikzpicture}
	\caption{Cross section of the RW2 prototype developed at the Swiss Plasma Center. In red the Cu stabilizer is highlighted while the yellow box indicates the superconductor. Jacket and cooling channel are both made of stainless steel.}
	\label{fig:RW2_Schliff}
\end{figure}

The design of the high-current winding pack design (and the prototype sample) will start with the selection and characterization of the Nb$_3$Sn strands. In order to predict the behavior of the superconductor during operation, the critical current density at $4.2\,$K, $12\,$T, and known strain needs to be known. This value is then used to calculate the needed superconductor cross-section for the target $I_{op}$ at the operation conditions: $6.5\,$K, $11.9\,$T and $-0.3\%$ strain. The superconductor cross-section and the strand Cu/non-Cu ratio determine the amount of copper needed for the stabilizer. Furthermore, for a prediction of AC losses, a full characterization of the strands in steady-state field and under variable field and temperature will provide the physical information needed. These measurements include magnetization cycles at different temperatures and applied magnet fields to better relate the theory from solid-state physics to the performances of the actual product purchased for this research work.

To ensure the mechanical stability of the TF coil during operation, a mechanical analysis is needed. From this analysis, the amount of steel in the jacket is established. Finally, thermal-hydraulic stability is investigated with dedicated studies. These will prove the hot spot temperature during a quench being within allowable limits and the pressure drop along the conductor affordable.

Once the right design is identified, a prototype needs to be tested in operation-like conditions which are provided by SULTAN. The production of such a prototype is also used to better understand the industrial processes in the production chain. Namely, an industrial task to study the longitudinal laser welding of a $1\,$km jacket demonstrator will be launched. This task aims at demonstrating the industrial feasibility of the production of R\&W conductors. Since the jacketing is done after heat treatment, it is not possible to pull the cable through the stainless steel jacket (ITER-like procedure). Therefore, the jacket is produced in two half profiles that need to be laser welded around the reacted cable. For large coils, as in DEMO, one conductor has a typical length of $1\,$km thus making an industrial feasibility study necessary for further development.

Developing a conductor with an operating current above $100\,$kA presents its own challenges like the very high electromagnetic loads during operation and thus, higher potential performance degradation. During this research, the use of pre-compression to mitigate the potential degradation of the conductor during operation will be explored. Furthermore, the test of such cable is not obvious: it needs a power supply with high enough current and voltage margins to reach the targeted operating current. Since in SULTAN the current to the sample is supplied by a superconducting transformer, this means that a very low resistance $(< 2\,\mathrm{n}\Omega)$ in the secondary loop is critical to reach a current of $105\,$kA. The tests in the SULTAN facility will include the determination of the critical current (I$_c$) and the current sharing temperature (T$_{cs}$) at the conductor nominal operation current I$_{op}$ and effective field up to $B~=~12\,$T. T$_{cs}$ is the temperature at which the superconductor is not capable anymore to transport all the current, and part of the current starts circulating through the copper in the strands and voltage develops. Cyclic loading and AC tests will also be performed in SULTAN.

%The demonstration of industrial laser welding feasibility is necessary for the React and Wind technology to be taken into consideration for the production of a large scale fusion device such as the EUROfusion DEMO. Most of all, the problem of on-line quality checks and pre-compression need to be addressed with the welding companies to assure high quality end product.

\newpage






